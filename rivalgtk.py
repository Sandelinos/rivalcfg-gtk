import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import rivalcfg
mouse = rivalcfg.get_first_mouse()

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Rivalcfg-gtk")

        # Box
        self.box = Gtk.Box(spacing=10)
        self.add(self.box)

        # Red
        self.red_button = Gtk.Button(label="Red")
        self.red_button.connect("clicked", self.red_clicked)
        self.box.pack_start(self.red_button, True, True, 0)

        # Green
        self.green_button = Gtk.Button(label="Green")
        self.green_button.connect("clicked", self.green_clicked)
        self.box.pack_start(self.green_button, True, True, 0)

        # Blue
        self.blue_button = Gtk.Button(label="Blue")
        self.blue_button.connect("clicked", self.blue_clicked)
        self.box.pack_start(self.blue_button, True, True, 0)

    def red_clicked(self, widget):
        mouse.set_logo_color("red")
        mouse.set_wheel_color("red")

    def green_clicked(self, widget):
        mouse.set_logo_color("green")
        mouse.set_wheel_color("green")

    def blue_clicked(self, widget):
        mouse.set_logo_color("blue")
        mouse.set_wheel_color("blue")

win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
